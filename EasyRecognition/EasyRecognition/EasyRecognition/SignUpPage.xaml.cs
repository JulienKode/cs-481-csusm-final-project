﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyRecognition
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignUpPage : ContentPage
	{
		public SignUpPage ()
		{
			InitializeComponent ();
		}

        async void Handle_signUp(object sender, EventArgs e)
        {
            string status = NetworkingClass.Instance.SignUp(Username.Text, Password.Text);
			if (status.Equals("OK"))
				App.Current.MainPage = new NavigationPage(new MainPage());
            else
                DisplayAlert("Oupss", status, "Ok");
        }

        async void Handle_signIn(object sender, EventArgs e)
        {
            string status = NetworkingClass.Instance.SignIn(Username.Text, Password.Text);
            if (status.Equals("OK"))
				App.Current.MainPage = new NavigationPage(new MainPage());
            else
                DisplayAlert("Oupss", status, "Ok");
        }
    }
}