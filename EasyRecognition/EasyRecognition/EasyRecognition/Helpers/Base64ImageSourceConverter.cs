﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using Xamarin.Forms;

namespace EasyRecognition
{
	public class Base64ImageSourceConverter : IValueConverter
    {
		public Base64ImageSourceConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var base64 = (string)value;
			Debug.WriteLine(base64.Length);
            if (base64 == null)
                return null;

            return ImageSource.FromStream(() =>
            {
                return new MemoryStream(System.Convert.FromBase64String(base64));
            });
		}
      
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
