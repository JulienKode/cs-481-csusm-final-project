﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EasyRecognition
{
    public static class Serialize
    {
        public static string ToJson(this List<User> self) => JsonConvert.SerializeObject(self, EasyRecognition.Converter.Settings);
    }
}
