﻿using System;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using RestSharp;
using Xamarin.Forms;
using QuickType;
using System.Collections.Generic;

namespace EasyRecognition
{
    public class NetworkingClass
    {
        private const string BasePathSign = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/";
        private const string BasePathRessource = "https://cs441-e704c.firebaseio.com/rest/saving-data/";
        private const string usersPath = "users/users.json";
        private const string imagePath = "image/image.json";
        private const string videoPath = "video/video.json";
        private const string FirebaseSecret = "AIzaSyBrXUnGaFGYmPqxGlfxtCiVYr0qVPYBGVI";
        private string idToken = null;
        private string refreshToken = null;
        private string localId = null;
        private static readonly NetworkingClass instance = new NetworkingClass();

        public static NetworkingClass Instance
        {
            get
            {
                return instance;
            }
        }

        private NetworkingClass() {
            if (Application.Current.Properties.ContainsKey("refreshToken")) {
                refreshToken = (string)Application.Current.Properties["refreshToken"];
            }
            if (Application.Current.Properties.ContainsKey("idToken"))
            {
                idToken = (string)Application.Current.Properties["idToken"];
            }
            if (Application.Current.Properties.ContainsKey("localId"))
            {
                localId = (string)Application.Current.Properties["localId"];
            }
        }

        public bool isConnected() {
            if (idToken != null && refreshToken != null && localId != null) {
                return true;
            }
            return false;
        }

        /**
         * Usage :
         * 
         * NetworkingClass.Instance.SignIn("toto@titi.com", "bite123");
         * 
         * ***/

        public string SignIn(string username, string password)
        {
            var client = new RestClient(BasePathSign + "verifyPassword?key=" + FirebaseSecret);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"email\":\""+ username + "\",\"password\":\""+ password +"\",\"returnSecureToken\":true}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var jobj = JObject.Parse(response.Content);
            return handleSign(jobj);
        }

        public string SignUp(string username, string password)
        {
            Debug.WriteLine(BasePathSign);
            Debug.WriteLine(FirebaseSecret);
            Debug.WriteLine(BasePathSign + "signupNewUser?key=" + FirebaseSecret);
            var client = new RestClient(BasePathSign + "signupNewUser?key=" + FirebaseSecret);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"email\":\"" + username + "\",\"password\":\"" + password + "\",\"returnSecureToken\":true}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var jobj = JObject.Parse(response.Content);
            return handleSign(jobj);
        }

        public string PostImage(ImageAnalysisModel image) {
            image.User = localId;
			image.CreatedAt = DateTime.Now;


            var client = new RestClient(BasePathRessource + imagePath);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", image.ToJson(), ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var jobj = JObject.Parse(response.Content);
            return handlePost(jobj);
       }

        public string handlePost(JObject response)
        {
            if (response["error"] != null)
            {
                var Message = response["error"]["message"].ToString();
                Debug.WriteLine(Message);
                return Message;
            }
            return "OK";
        }

        public string handleSign(JObject response) {
            Debug.WriteLine(response);
            if (response["error"] != null)
            {
                var Message = response["error"]["message"].ToString();
                Debug.WriteLine(Message);
                return Message;
            }
            idToken = response["idToken"].ToString();
            refreshToken = response["refreshToken"].ToString();
            localId = response["localId"].ToString();
			Application.Current.Properties["localId"] = localId;
            Application.Current.Properties["idToken"] = idToken;
            Application.Current.Properties["refreshToken"] = refreshToken;
            Application.Current.SavePropertiesAsync();
            return "OK";
        }
        
		public List<ImageAnalysisModel> GetHistoric()
        {
			Debug.WriteLine("GetHistoric");
			var client = new RestClient(BasePathRessource + imagePath + Uri.EscapeUriString(String.Format("?orderBy=\"user\"&equalTo=\"{0}\"", localId)) );
            var request = new RestRequest(Method.GET);
	
            IRestResponse response = client.Execute(request);
			var arr = new List<ImageAnalysisModel>();
			var jobj = JObject.Parse(response.Content);
            foreach (var x in jobj)
            {
                string name = x.Key;
                JToken value = x.Value;
				arr.Add(ImageAnalysisModel.FromJson(value.ToString()));
            }
			arr.Sort((ps1, ps2) => DateTime.Compare(ps2.CreatedAt, ps1.CreatedAt));
			return arr;
            //return handleSign(jobj);
        }
    }
}
