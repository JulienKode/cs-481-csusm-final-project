﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyRecognition
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ThirdItem : ContentPage
	{
		public ThirdItem ()
		{
			InitializeComponent ();
		}

        public void Handle_Logout(object sender, EventArgs e)
        {
            Application.Current.Properties["refreshToken"] = null;
            Application.Current.Properties["idToken"] = null;
            Application.Current.Properties["localId"] = null;
            App.Current.MainPage = new SignUpPage();
        }

    }
}