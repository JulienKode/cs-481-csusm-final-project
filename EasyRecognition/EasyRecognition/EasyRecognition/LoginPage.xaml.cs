﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyRecognition
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}

        public void Handle_login(object sender, EventArgs e)
        {
            string status = NetworkingClass.Instance.SignIn(Username.Text, Password.Text);
            if (status.Equals("OK"))
                App.Current.MainPage = new MainPage();
            else
                DisplayAlert("Oupss", "Something wrong happened, please verify your credentials.", "Ok");
        }

        public void Handle_signup(object sender, EventArgs e)
        {
            App.Current.MainPage = new SignUpPage();
        }
    }
}