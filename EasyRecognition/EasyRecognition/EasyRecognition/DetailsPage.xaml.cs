﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EasyRecognition
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailsPage : ContentPage
	{
        public DetailsPage (List<string> imageAnalyze, byte[] image64)
		{
			InitializeComponent ();
            var result = String.Join(", ", imageAnalyze.ToArray());
            attribute.Text = result; 
            image.Source = ImageSource.FromStream(() => new MemoryStream(image64));
        }

    }
}