﻿using Clarifai.API;
using Clarifai.DTOs.Inputs;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using QuickType;
using System.Threading.Tasks;

namespace EasyRecognition
{

    class RecognitionCall
    {
        private static readonly RecognitionCall instance = new RecognitionCall();
        private ClarifaiClient client;

        public static RecognitionCall Instance
        {
            get
            {
                return instance;
            }
        }

        private RecognitionCall()
        {
            client = new ClarifaiClient("d575b467253e4a5ebba0b7fcd6937c9b");
        }

        public async void Predict_Video_By_URL(string url)
        {
           var response =  await client.PublicModels.GeneralVideoModel.Predict(
            new ClarifaiURLVideo(url))
            .ExecuteAsync();

        }
        /*
        public async void Predict_Photo_By_URL(string url)
        {
			
            var response = await client.PublicModels.GeneralModel.Predict(
				new ClarifaiFileImage(File.ReadAllBytes()))
                .ExecuteAsync();

            Debug.WriteLine(response);
        }
        */

		public async Task<List<AnalysisResult>> Predict_Photo_By_File(string AsBase64String)
        {
            var response = await client.PublicModels.GeneralModel.Predict(
				new ClarifaiFileImage(Convert.FromBase64String(AsBase64String)))
                .ExecuteAsync();
			//Debug.WriteLine(response.RawBody);
			var jobj = JObject.Parse(response.RawBody);

			//Debug.WriteLine(jobj.SelectToken("outputs"));
			Debug.WriteLine(jobj.SelectToken("outputs[0].data.concepts"));


			var concepts = (JArray)jobj.SelectToken("outputs[0].data.concepts");
      
            
			var arr = new List<AnalysisResult>();
			if (concepts == null) {
				return arr;
			}
			foreach (JObject value in concepts)
            {
				arr.Add(AnalysisResult.FromJson(value.ToString()));
            }
			Debug.WriteLine(arr);
			return arr;
        }

        public async void Predict_Video_By_File(string path)
        {
            var response = await client.PublicModels.GeneralModel.Predict(
                new ClarifaiFileVideo(File.ReadAllBytes(path)))
                .ExecuteAsync();
        }

		public void AnalysisImage(string image) {

			const string ACCESS_TOKEN = "d575b467253e4a5ebba0b7fcd6937c9b";
            const string CLARIFAI_API_URL = "https://api.clarifai.com/v2/models/{model}/outputs";

			var client = new RestClient(CLARIFAI_API_URL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Key " + ACCESS_TOKEN);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{" +
                    "\"inputs\": [" +
                        "{" +
                            "\"data\": {" +
                                "\"image\": {" +
                                    "\"base64\": \"" + image + "\"" +
                                "}" +
                           "}" +
                        "}" +
                    "]" +
                "}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var jobj = JObject.Parse(response.Content);

            //Debug.WriteLine(body);
            Debug.WriteLine(JObject.Parse(response.Content));
			/*
            
            using (HttpClient client = new HttpClient())
            {
                // Set the authorization header
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + ACCESS_TOKEN);

                // The JSON to send in the request that contains the encoded image data
                // Read the docs for more information - https://developer.clarifai.com/guide/predict#predict
                HttpContent json = new StringContent(
                    "{" +
                        "\"inputs\": [" +
                            "{" +
                                "\"data\": {" +
                                    "\"image\": {" +
                                        "\"base64\": \"" + image + "\"" +
                                    "}" +
                               "}" +
                            "}" +
                        "]" +
                    "}", Encoding.UTF8, "application/json");

                // Send the request to Clarifai and get a response
                var response = client.PostAsync(CLARIFAI_API_URL, json).Result;

                // Check the status code on the response
                if (!response.IsSuccessStatusCode)
                {
                    // End here if there was an error
                    return;
                }

                // Get response body
                string body = response.Content.ReadAsStringAsync().Result.ToString();



            }
                */
		}
    }
}
