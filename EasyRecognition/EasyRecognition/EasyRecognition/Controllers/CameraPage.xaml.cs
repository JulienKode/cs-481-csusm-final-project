﻿using System;
using System.Collections.Generic;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using QuickType;
using System.Diagnostics;
using System.IO;
using Plugin.Toasts;
using Plugin.Toasts.Options;

namespace EasyRecognition
{
    public partial class CameraPage : ContentPage
    {

		private ImageAnalysisModel imageData = new ImageAnalysisModel();
   
        public CameraPage()
        {
            InitializeComponent();

            myMedia.Clicked += async (sender, args) =>
            {
                chooseMedia();
            };

			uploadMedia.Clicked += async (sender, args) =>
            {
				if (imageData.Image == null) {
					// Alert select an image before
					DisplayAlert("Alert", "Please choose an image before", "OK");
					return;
				}
				DisplayAlert("Alert", "Upload in progress", "OK");
				imageData.Concepts = await RecognitionCall.Instance.Predict_Photo_By_File(imageData.Image);
                NetworkingClass.Instance.PostImage(imageData);
				image.Source = null;

				var notificator = DependencyService.Get<IToastNotificator>();

                var options = new NotificationOptions()
                {
                    Title = "Congrats",
                    Description = "Your image has been uploaded, go to the historic page to see the result",
					IsClickable = true,
                    WindowsOptions = new WindowsOptions() { LogoUri = "icon.png" },
                    ClearFromHistory = false,
                    AllowTapInNotificationCenter = false
                };

				var result = await notificator.Notify(options);
            };
        }
        
        async void takePhoto() {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Front
            });

            if (file == null)
                return;


            var stream = file.GetStream();
            ImageToBase64(stream);
            file.Dispose();
        }

        private async void pickPhoto() {
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,

            });


            if (file == null)
                return;


			var stream = file.GetStream();
            ImageToBase64(stream);
            file.Dispose();
        }

        private async void ImageToBase64(System.IO.Stream stream) {
            Debug.WriteLine("HEU");
            var bytes = new byte[stream.Length];
            await stream.ReadAsync(bytes, 0, (int)stream.Length);
            string base64 = System.Convert.ToBase64String(bytes);
            imageData.Image = base64;
            Debug.WriteLine(imageData.Image);

            byte[] Base64Stream = Convert.FromBase64String(base64);
			image.Source = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
            
        }


        private async void takeVideo() {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
            {
                DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakeVideoAsync(new Plugin.Media.Abstractions.StoreVideoOptions
            {
                Name = "video.mp4",
                Directory = "DefaultVideos",
            });

            if (file == null)
                return;

            DisplayAlert("Video Recorded", "Location: " + file.Path, "OK");

            file.Dispose();
        }

        private async void pickVideo() {
            if (!CrossMedia.Current.IsPickVideoSupported)
            {
                DisplayAlert("Videos Not Supported", ":( Permission not granted to videos.", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickVideoAsync();

            if (file == null)
                return;

            DisplayAlert("Video Selected", "Location: " + file.Path, "OK");
            file.Dispose();
        }

        private async void chooseMedia()
        {
            var actionSheet = await DisplayActionSheet("Where you want to take your media ?", "Cancel", null, "Pick Photo", "Take Photo");

            switch (actionSheet)
            {
                case "Cancel":

                    break;
                case "Pick Photo":
                    pickPhoto();
                    break;
                case "Take Photo":
                    takePhoto();
                   break;
                case "Pick Video":
                    pickVideo();
                    break;
                case "Take Video":
                    takeVideo();
                    break;

            }

        }
    }
}
