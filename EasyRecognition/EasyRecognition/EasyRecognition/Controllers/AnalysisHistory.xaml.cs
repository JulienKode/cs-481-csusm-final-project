﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using QuickType;
using Xamarin.Forms;

namespace EasyRecognition
{
    public partial class AnalysisHistory : ContentPage
    {
        public AnalysisHistory()
        {
            InitializeComponent();
			PopulateListView();
        }

		void PopulateListView() {
			var imagesCollection = new ObservableCollection<ImageAnalysisModel>();
			var images = NetworkingClass.Instance.GetHistoric();
			foreach (var item in images)
				imagesCollection.Add(item);
			ListViewWithCustomCells.ItemsSource = imagesCollection;
		}

		async void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            ImageAnalysisModel itemTapped = (ImageAnalysisModel)listView.SelectedItem;
            List<string> conceptName = new List<string>();
            foreach (AnalysisResult concept in itemTapped.Concepts)
            {
                conceptName.Add(concept.Name);
            }
            byte[] Base64Stream = Convert.FromBase64String(itemTapped.Image);
			await Navigation.PushAsync(new DetailsPage(conceptName, Base64Stream));
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
		{
			PopulateListView();
			ListViewWithCustomCells.IsRefreshing = false;
		}

		void Handle_ContextMenuMoreButton(object sender, System.EventArgs e)
		{
		}

		void Handle_ContextMenuDeleteButton(object sender, System.EventArgs e)
		{
		}

		void Handle_SwitchToggled(object sender, Xamarin.Forms.ToggledEventArgs e)
		{
		}
    }
}
