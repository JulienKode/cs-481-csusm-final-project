﻿namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
   
    public static class Serialize
    {
        public static string ToJson(this AnalysisResult self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
		public static string ToJson(this ImageAnalysisModel self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
        
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
