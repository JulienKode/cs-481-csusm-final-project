﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var welcome = Welcome.FromJson(jsonString);

namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class ImageAnalysisModel
    {
        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

		[JsonProperty("concepts")]
		public List<AnalysisResult> Concepts { get; set; }
        
		[JsonProperty("createdAt")]
		public DateTime CreatedAt { get; set; }
    }

    public partial class ImageAnalysisModel
    {
        public static ImageAnalysisModel FromJson(string json) => JsonConvert.DeserializeObject<ImageAnalysisModel>(json, QuickType.Converter.Settings);
    }

}
