﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var welcome = Welcome.FromJson(jsonString);

namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class AnalysisResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("app_id")]
        public string AppId { get; set; }
    }

	public partial class AnalysisResult
    {
		public static AnalysisResult FromJson(string json) => JsonConvert.DeserializeObject<AnalysisResult>(json, QuickType.Converter.Settings);
    }
  
}
